import os
import math
import numpy as np
from data_loader import *
from cal_eer import *
import argparse

pi = np.array(np.pi)

def LoadPLDA(mdl_plda):
    f = open(mdl_plda, 'r')
    lines = f.readlines()
    f.close()

    dim = len(lines[2].strip().split())
    # print(dim)

    plda_W = []
    for line in lines:
        part = line.strip().split()
        if len(part) == (dim + 3):
            plda_b = np.array(part[2:-1], dtype=np.float)
        elif len(part) == dim:
            plda_W.append(np.array(part, dtype=np.float))
        elif len(part) == (dim + 1):
            plda_W.append(np.array(part[:-1], dtype=np.float))
        elif len(part) == (dim + 2):
            plda_SB = np.array(part[1:-1], dtype=np.float)
        else:
            continue
    return plda_b, np.array(plda_W), plda_SB


def GetNormalizationFactor(transformed_vector, num_utts, plda_SB):
  assert(num_utts > 0)
  dim = len(transformed_vector)
  inv_covar = 1.0 / (1.0 / num_utts + plda_SB)
  dot_prod = np.dot(inv_covar, transformed_vector ** 2)
  return math.sqrt(dim / dot_prod)


def Normalize(vector, num_utts, plda_SB, plda_dim, simple_length_norm=True):
  dim = len(vector)
  normalization_factor = 0.0
  if simple_length_norm:
      normalization_factor = math.sqrt(dim) / np.linalg.norm(vector)
  else:
      normalization_factor = GetNormalizationFactor(vector, num_utts, plda_SB[:plda_dim])
  normalized_vector = vector * normalization_factor
  return normalized_vector


def NLScore(enroll_vec, enroll_num, test_vec, SB, SW):
    '''
    normalized likelihood with uncertain means
    SB is the speaker between var
    SW is the speaker within var
    '''
    # uk = enroll_vec * (enroll_num * SB / (enroll_num * SB + SW))
    # pk = ((test_vec - uk)**2 / (SW + SB * SW / (enroll_num * SB + SW))).sum()
    # px = (test_vec**2 / (SW + SB)).sum()

    uk = enroll_vec * (enroll_num * SB / (enroll_num * SB + SW))
    vk = SW + SB * SW / (enroll_num * SB + SW)
    pk = ((test_vec - uk)**2 / vk).sum() + np.log(2 * pi * vk).sum()
    px = (test_vec**2 / (SW + SB)).sum() + np.log(2 * pi * (SW + SB)).sum()

    score = 0.5 * (px - pk)
    return score


def score_by_trials(enroll_npz, enroll_num_utts, test_npz, test_trials, score_file, plda_SB, plda_dim, normalization):
    '''
    compute NL scores by trials
    '''
    # load data
    print("Load data")
    enroll_vectors = np.load(enroll_npz)['vectors']
    enroll_spkers = np.load(enroll_npz)['spker_label']
    enroll_utters = np.load(enroll_npz)['utt_label']

    test_vectors = np.load(test_npz)['vectors']
    test_spkers = np.load(test_npz)['spker_label']
    test_utters = np.load(test_npz)['utt_label']

    # build hashmap enroll_spk -> utters
    enroll_spk2utt = {}
    for idx in range(len(enroll_spkers)):
        spk = enroll_spkers[idx]
        if "byte" in str(type(spk)):
            spk = spk.decode("utf-8")
        if spk not in enroll_spk2utt:
            enroll_spk2utt[spk] = []
        enroll_spk2utt[spk].append(enroll_vectors[idx])

    # build hashmap test_utt -> utter
    test_spk2utt = {}
    for idx in range(len(test_utters)):
        label = test_utters[idx]
        test_spk2utt[label] = test_vectors[idx]

    # load trials and compute EER
    enroll_id, test_id, target_id = load_trials(test_trials)
    num_utts = load_num_utts(enroll_num_utts)


    enroll_dict = {}
    for idx in range(len(enroll_spkers)):
        spk = enroll_spkers[idx]
        if "byte" in str(type(spk)):
            spk = spk.decode("utf-8")
        enroll_vecs = enroll_spk2utt[spk]
        enroll_vec = np.mean(np.array(enroll_vecs), axis=0)
        if normalization:
            enroll_vec = Normalize(enroll_vec, num_utts[spk], plda_SB, plda_dim)
        enroll_dict[spk] = enroll_vec

    test_dict = {}
    for idx in range(len(test_utters)):
        label = test_utters[idx]
        test_vec = test_spk2utt[label]
        if normalization:
            test_vec = Normalize(test_vec, 1, plda_SB, plda_dim)
        test_dict[label] = test_vec

    target_scores = []
    nontarget_scores = []
    foo = open(score_file, 'w')
    for i in range(len(target_id)):
        enroll_num = num_utts[enroll_id[i]]
        enroll_vec = enroll_dict[enroll_id[i]]
        test_vec = test_dict[test_id[i]]
        score = NLScore(enroll_vec, enroll_num, test_vec, plda_SB[:plda_dim], 1)
        foo.write(' '.join([enroll_id[i], test_id[i], str(score)]) + '\n')

        if target_id[i]:
            target_scores.append(score)
        else:
            nontarget_scores.append(score)

    EER, thres = compute_eer(target_scores, nontarget_scores)
    print("EER: {:.3f}% and Threshold: {:.3f}".format(EER*100.0, thres))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--enroll-npz', default='enroll/xvector.npz', help='npz file of enroll vector')
    parser.add_argument(
        '--enroll-num-utts', default='enroll/num_utts.ark', help='mapping file of spker to utter number')
    parser.add_argument(
        '--test-npz', default='test/xvector.npz', help='npz file of test vector')
    parser.add_argument(
        '--trials', default='trials.trl', help='file of test trials')
    parser.add_argument(
        '--score', default='score.foo', help='file of trial scores')
    parser.add_argument(
        '--plda', default='plda', help='file of plda model in Kaldi')
    parser.add_argument(
        '--plda-dim', type=int, default=150, help='dim of PLDA')
    parser.add_argument(
        '--normalization', action='store_true', default=False, help='process length normlization')

    args = parser.parse_args()

    if not os.path.exists(os.path.dirname(args.score)):
        os.makedirs(os.path.dirname(args.score))

    _, _, plda_SB = LoadPLDA(args.plda)
    score_by_trials(args.enroll_npz, args.enroll_num_utts, args.test_npz, args.trials, args.score, plda_SB, args.plda_dim, args.normalization)

