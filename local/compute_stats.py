import os
import numpy as np
from scipy.spatial.distance import cdist
import argparse

def LoadPLDA(mdl_plda):
    f = open(mdl_plda, 'r')
    lines = f.readlines()
    f.close()

    dim = len(lines[2].strip().split())
    # print(dim)

    plda_W = []
    for line in lines:
        part = line.strip().split()
        if len(part) == (dim + 3):
            plda_b = np.array(part[2:-1], dtype=np.float)
        elif len(part) == dim:
            plda_W.append(np.array(part, dtype=np.float))
        elif len(part) == (dim + 1):
            plda_W.append(np.array(part[:-1], dtype=np.float))
        elif len(part) == (dim + 2):
            plda_SB = np.array(part[1:-1], dtype=np.float)
        else:
            continue
    return np.array(plda_W)


def similarity(mat_A, mat_B, metric, saver):
    dists = cdist(mat_A, mat_B, metric=metric)
    dists = dists.diagonal()
    with open(saver, 'a') as f:
        for i in range(len(dists)):
            f.write(str(dists[i]) + ' ')
        f.write('\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--plda-A', default='plda_A', help='file of plda model in Kaldi')
    parser.add_argument(
        '--plda-B', default='plda_B', help='file of plda model in Kaldi')
    parser.add_argument(
        '--metric', default='euclidean', help='euclidean | cosine')
    parser.add_argument(
        '--saver', default='dis.log', help='log file')

    args = parser.parse_args()

    mat_A = LoadPLDA(args.plda_A)
    mat_B = LoadPLDA(args.plda_B)
    
    similarity(mat_A, mat_B, args.metric, args.saver)

