#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e

stage=0

case=aging
nnet_dir=$1

# Baseline: Create a PLDA model and do scoring.
if [ $stage -le 0 ]; then
  for aging in 1st 1st-2nd 1st-3rd 1st-4th 1st-5th 1st-6th 1st-7th 1st-8th; do
    echo $aging
    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_${aging}_dev \
        $nnet_dir/$case/xvectors_1st_enroll \
        $nnet_dir/$case/xvectors_${aging}_test \
        data/$case/$aging/test/trials \
        scores/$case/$aging
  done
fi


# kaldi to npz
if [ $stage -le 1 ]; then
  for aging in 1st 1st-2nd 1st-3rd 1st-4th 1st-5th 1st-6th 1st-7th 1st-8th; do
    for sub in dev enroll test; do
      python -u local/kaldi2npz.py \
             --is-eval \
             --src-file $nnet_dir/$case/xvectors_${aging}_${sub}/xvector.scp \
             --dest-file $nnet_dir/$case/xvectors_${aging}_${sub}/xvector.npz \
             --utt2spk-file $nnet_dir/$case/xvectors_${aging}_${sub}/utt2spk
    done
  done
fi


# Baseline: NL scoring.
if [ $stage -le 2 ]; then
  echo "Baseline: NL scoring."
  for aging in 1st 1st-2nd 1st-3rd 1st-4th 1st-5th 1st-6th 1st-7th 1st-8th; do
    echo $aging
    if [ ! -f $nnet_dir/$case/xvectors_${aging}_dev/plda_ ]; then
      ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${aging}_dev/plda $nnet_dir/$case/xvectors_${aging}_dev/plda_
    fi

    python -u local/score/plda_score_by_trials.py \
           --enroll-npz $nnet_dir/$case/xvectors_1st_enroll/xvector.npz \
           --enroll-num-utts $nnet_dir/$case/xvectors_1st_enroll/num_utts.ark \
           --test-npz $nnet_dir/$case/xvectors_${aging}_test/xvector.npz \
           --trials data/$case/$aging/test/trials \
           --global-mean $nnet_dir/$case/xvectors_${aging}_dev/mean.vec \
           --plda $nnet_dir/$case/xvectors_${aging}_dev/plda_ \
           --plda-dim 512
  done
fi
