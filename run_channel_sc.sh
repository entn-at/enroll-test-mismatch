#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e
fbankdir=`pwd`/_fbank
mfccdir=`pwd`/_mfcc
vaddir=`pwd`/_vad

stage=1

case=channel
nnet_dir=$1
checkpoint='last'


# Make MFCCs and compute the energy-based VAD for each dataset
if [ $stage -le 1 ]; then
  for device in Android Mic iOS; do
    for sub in enroll test; do
      echo $device/$sub
      steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 10 --cmd "$cmd" \
        data/$case/$device/$sub exp/make_mfcc $mfccdir/$device
      utils/fix_data_dir.sh data/$case/$device/$sub

      sid/compute_vad_decision.sh --vad-config conf/vad.conf --nj 10 --cmd "$cmd" \
        data/$case/$device/$sub exp/make_vad $vaddir/$device
      utils/fix_data_dir.sh data/$case/$device/$sub
      rm -r data/$case/$device/$sub/feats.scp
      rm -r $mfccdir

      steps/make_fbank.sh --fbank-config conf/fbank.conf --nj 10 --cmd "$cmd" \
        data/$case/$device/$sub exp/make_fbank $fbankdir/$device
      utils/fix_data_dir.sh data/$case/$device/$sub
    done
  done
fi


# Extract the embeddings
if [ $stage -le 2 ]; then
  for device in Android Mic iOS; do
    for sub in enroll test; do
      echo $device/$sub
      nnet/run_extract_embeddings.sh --cmd "$cmd" --nj 10 \
        --use-gpu false --checkpoint $checkpoint --stage 0 \
        --chunk-size 10000 --normalize false --node "tdnn6_dense" \
        $nnet_dir data/$case/$device/$sub $nnet_dir/$case/xvectors_${device}_${sub}
      cp data/$case/$device/$sub/{spk2utt,utt2spk} $nnet_dir/$case/xvectors_${device}_${sub}/
    done
  done
fi


# Baseline: Create a PLDA model and do scoring.
if [ $stage -le 3 ]; then
  for enroll_device in Android Mic iOS; do
    for test_device in Android Mic iOS; do
      echo $enroll_device to $test_device

      local/score/plda_scoring.sh --normalize-length true --simple-length-norm true \
          xvector.scp \
          $nnet_dir/$case/xvectors_${enroll_device}_enroll \
          $nnet_dir/$case/xvectors_${enroll_device}_enroll \
          $nnet_dir/$case/xvectors_${test_device}_test \
          data/$case/$test_device/test/trials \
          scores/$case/${enroll_device}-${test_device}

    done
  done
fi


# kaldi to npz
if [ $stage -le 4 ]; then
  for device in Android Mic iOS; do
    for sub in enroll test; do
      python -u local/kaldi2npz.py \
             --is-eval \
             --src-file $nnet_dir/$case/xvectors_${device}_${sub}/xvector.scp \
             --dest-file $nnet_dir/$case/xvectors_${device}_${sub}/xvector.npz \
             --utt2spk-file $nnet_dir/$case/xvectors_${device}_${sub}/utt2spk
    done
  done
fi


# Baseline: NL scoring.
if [ $stage -le 5 ]; then
  echo "Baseline: NL scoring."
  for dim in 14 512; do
    echo $dim
    for enroll_device in Android Mic iOS; do
      for test_device in Android Mic iOS; do
        echo $enroll_device to $test_device
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_device}-${test_device}
        python -u local/score/plda_score_by_trials.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_device}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_device}_enroll/num_utts.ark \
               --test-npz $nnet_dir/$case/xvectors_${test_device}_test/xvector.npz \
               --trials data/$case/$test_device/test/trials \
               --score scores/$case/$dim/${enroll_device}-${test_device}/plda_scores \
               --global-mean $nnet_dir/$case/xvectors_${enroll_device}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm

      done
    done
  done
fi


# Special Case 1: x' = x + b.
if [ $stage -le 6 ]; then
  echo "Special Case 1: x' = x + b."
  for dim in 14 512; do
    echo $dim
    for enroll_device in Android Mic iOS; do
      for test_device in Android Mic iOS; do
        echo $enroll_device to $test_device
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_device}-${test_device}
        python -u local/score/plda_score_by_trials_bias.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_device}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_device}_enroll/num_utts.ark \
               --test-npz $nnet_dir/$case/xvectors_${test_device}_test/xvector.npz \
               --trials data/$case/$test_device/test/trials \
               --score scores/$case/$dim/${enroll_device}-${test_device}/plda_scores \
               --global-mean-enroll $nnet_dir/$case/xvectors_${enroll_device}_enroll/mean.vec \
               --global-mean-test $nnet_dir/$case/xvectors_${test_device}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm
      done
    done
  done
fi


# Special Case 2: scenarios sw'.
if [ $stage -le 7 ]; then
  echo "Special Case 2: scenarios sw'."
  for dim in 14 512; do
    echo $dim
    for enroll_device in Android Mic iOS; do
      for test_device in Android Mic iOS; do
        echo $enroll_device to $test_device
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_device}-${test_device}
        python -u local/score/plda_score_by_trials_sw.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_device}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_device}_enroll/num_utts.ark \
               --dev-npz $nnet_dir/$case/xvectors_${test_device}_enroll/xvector.npz \
               --test-npz $nnet_dir/$case/xvectors_${test_device}_test/xvector.npz \
               --trials data/$case/$test_device/test/trials \
               --score scores/$case/$dim/${enroll_device}-${test_device}/plda_scores \
               --global-mean $nnet_dir/$case/xvectors_${enroll_device}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm

      done
    done
  done
fi


# Special Case 2: spk-dependent sw'.
if [ $stage -le 8 ]; then
  echo "Special Case 2: spk-dependent sw'."
  for dim in 14 512; do
    echo $dim
    for enroll_device in Android Mic iOS; do
      for test_device in Android Mic iOS; do
        echo $enroll_device to $test_device
        if [ ! -f $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ ]; then
          echo 'format to text'
          ivector-copy-plda --binary=false $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_
        fi
  
        mkdir -p scores/$case/$dim/${enroll_device}-${test_device}
        python -u local/score/plda_score_by_trials_sw_spk.py \
               --enroll-npz $nnet_dir/$case/xvectors_${enroll_device}_enroll/xvector.npz \
               --enroll-num-utts $nnet_dir/$case/xvectors_${enroll_device}_enroll/num_utts.ark \
               --dev-npz $nnet_dir/$case/xvectors_${test_device}_enroll/xvector.npz \
               --test-npz $nnet_dir/$case/xvectors_${test_device}_test/xvector.npz \
               --trials data/$case/$test_device/test/trials \
               --score scores/$case/$dim/${enroll_device}-${test_device}/plda_scores \
               --global-mean $nnet_dir/$case/xvectors_${enroll_device}_enroll/mean.vec \
               --plda $nnet_dir/$case/xvectors_${enroll_device}_enroll/plda_ \
               --plda-dim $dim \
               --normalize-length \
               --simple-length-norm
      done
    done
  done
fi

