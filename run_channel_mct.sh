#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e

stage=3

case=channel
nnet_dir=$1

# Baseline: Create a PLDA model and do scoring.
if [ $stage -le 0 ]; then
  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_Android_iOS_dev \
      $nnet_dir/$case/xvectors_Android_enroll \
      $nnet_dir/$case/xvectors_iOS_test \
      data/$case/iOS/test/trials \
      scores/$case/Android_iOS_MCT

  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_Android_iOS_dev \
      $nnet_dir/$case/xvectors_iOS_enroll \
      $nnet_dir/$case/xvectors_Android_test \
      data/$case/Android/test/trials \
      scores/$case/iOS_Android_MCT

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_Mic_dev \
        $nnet_dir/$case/xvectors_Android_enroll \
        $nnet_dir/$case/xvectors_Mic_test \
        data/$case/Mic/test/trials \
        scores/$case/Android_Mic_MCT

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_Mic_dev \
        $nnet_dir/$case/xvectors_Mic_enroll \
        $nnet_dir/$case/xvectors_Android_test \
        data/$case/Android/test/trials \
        scores/$case/Mic_Android_MCT

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_iOS_Mic_dev \
        $nnet_dir/$case/xvectors_iOS_enroll \
        $nnet_dir/$case/xvectors_Mic_test \
        data/$case/Mic/test/trials \
        scores/$case/iOS_Mic_MCT

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_iOS_Mic_dev \
        $nnet_dir/$case/xvectors_Mic_enroll \
        $nnet_dir/$case/xvectors_iOS_test \
        data/$case/iOS/test/trials \
        scores/$case/Mic_iOS_MCT
fi


# Baseline: Create a PLDA model and do scoring.
if [ $stage -le 2 ]; then
  for i in 0 20 40 60 80 100; do
    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_Mic_${i}_dev \
        $nnet_dir/$case/xvectors_Android_enroll \
        $nnet_dir/$case/xvectors_Mic_test \
        data/$case/Mic/test/trials \
        scores/$case/Android_Mic

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_iOS_${i}_dev \
        $nnet_dir/$case/xvectors_Android_enroll \
        $nnet_dir/$case/xvectors_iOS_test \
        data/$case/iOS/test/trials \
        scores/$case/Android_iOS
  
    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_Mic_${i}_dev \
        $nnet_dir/$case/xvectors_Mic_enroll \
        $nnet_dir/$case/xvectors_Android_test \
        data/$case/Android/test/trials \
        scores/$case/Mic_Android

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_iOS_Mic_${i}_dev \
        $nnet_dir/$case/xvectors_Mic_enroll \
        $nnet_dir/$case/xvectors_iOS_test \
        data/$case/iOS/test/trials \
        scores/$case/Mic_iOS

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_iOS_${i}_dev \
        $nnet_dir/$case/xvectors_iOS_enroll \
        $nnet_dir/$case/xvectors_Android_test \
        data/$case/Android/test/trials \
        scores/$case/iOS_Android

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_iOS_Mic_${i}_dev \
        $nnet_dir/$case/xvectors_iOS_enroll \
        $nnet_dir/$case/xvectors_Mic_test \
        data/$case/Mic/test/trials \
        scores/$case/iOS_Mic
  done
fi


# Baseline: Create a PLDA model and do scoring.
if [ $stage -le 3 ]; then
  for i in 20 40 60 80 100; do
    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_Mic_${i}_2_dev \
        $nnet_dir/$case/xvectors_Android_enroll \
        $nnet_dir/$case/xvectors_Mic_test \
        data/$case/Mic/test/trials \
        scores/$case/Android_Mic

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_iOS_${i}_2_dev \
        $nnet_dir/$case/xvectors_Android_enroll \
        $nnet_dir/$case/xvectors_iOS_test \
        data/$case/iOS/test/trials \
        scores/$case/Android_iOS
  
    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_Mic_${i}_2_dev \
        $nnet_dir/$case/xvectors_Mic_enroll \
        $nnet_dir/$case/xvectors_Android_test \
        data/$case/Android/test/trials \
        scores/$case/Mic_Android

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_iOS_Mic_${i}_2_dev \
        $nnet_dir/$case/xvectors_Mic_enroll \
        $nnet_dir/$case/xvectors_iOS_test \
        data/$case/iOS/test/trials \
        scores/$case/Mic_iOS

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_Android_iOS_${i}_2_dev \
        $nnet_dir/$case/xvectors_iOS_enroll \
        $nnet_dir/$case/xvectors_Android_test \
        data/$case/Android/test/trials \
        scores/$case/iOS_Android

    local/score/plda_scoring.sh \
        xvector.scp \
        $nnet_dir/$case/xvectors_iOS_Mic_${i}_2_dev \
        $nnet_dir/$case/xvectors_iOS_enroll \
        $nnet_dir/$case/xvectors_Mic_test \
        data/$case/Mic/test/trials \
        scores/$case/iOS_Mic
  done
fi

