# Enroll-Test Mismatch

## Dependency
```
pip3 install -r local/requrements.txt
```

```
Kaldi >= 5.4
```

```
tf-kaldi
gitlab: https://gitlab.com/csltstu/tf-kaldi-speaker
```


## Data
data preparation from kaldi to npz
```
python -u local/kaldi2npz.py
```


## Recipe
```
Cross-channel
sh run_channel.sh
```

```
Near-far field
sh run_near-far.sh
```

```
Time aging
sh run_aging.sh
```


## Tensorboard
monitor the training process.
```
tensorboard --logdir runs/*
```

