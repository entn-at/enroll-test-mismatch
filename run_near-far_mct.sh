#!/bin/bash

. ./cmd.sh
. ./path.sh

set -e

stage=0

case=near-far
nnet_dir=$1

if [ $stage -le 0 ]; then
  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_1m_3m_dev \
      $nnet_dir/$case/xvectors_1m_enroll \
      $nnet_dir/$case/xvectors_3m_test \
      data/$case/3m/test/trials \
      scores/$case/1m_3m_MCT

  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_1m_3m_dev \
      $nnet_dir/$case/xvectors_3m_enroll \
      $nnet_dir/$case/xvectors_1m_test \
      data/$case/1m/test/trials \
      scores/$case/3m_1m_MCT

  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_1m_5m_dev \
      $nnet_dir/$case/xvectors_1m_enroll \
      $nnet_dir/$case/xvectors_5m_test \
      data/$case/5m/test/trials \
      scores/$case/1m_5m_MCT

  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_1m_5m_dev \
      $nnet_dir/$case/xvectors_5m_enroll \
      $nnet_dir/$case/xvectors_1m_test \
      data/$case/1m/test/trials \
      scores/$case/5m_1m_MCT

  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_3m_5m_dev \
      $nnet_dir/$case/xvectors_3m_enroll \
      $nnet_dir/$case/xvectors_5m_test \
      data/$case/5m/test/trials \
      scores/$case/3m_5m_MCT

  local/score/plda_scoring.sh \
      xvector.scp \
      $nnet_dir/$case/xvectors_3m_5m_dev \
      $nnet_dir/$case/xvectors_5m_enroll \
      $nnet_dir/$case/xvectors_3m_test \
      data/$case/3m/test/trials \
      scores/$case/5m_3m_MCT
fi

