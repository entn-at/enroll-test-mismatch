import math
import numpy as np
import torch
import torch.nn as nn

pi = torch.from_numpy(np.array(np.pi))

class LinearLayer(nn.Module):
    def __init__(self, num_inputs):
        super(LinearLayer, self).__init__()

        self.num_inputs = num_inputs
        self.tri1 = nn.Parameter(torch.eye(num_inputs, num_inputs), requires_grad=True)
        self.tri2 = nn.Parameter(torch.eye(num_inputs, num_inputs), requires_grad=True)
        self.b = nn.Parameter(torch.zeros(num_inputs), requires_grad=True)

    def forward(self, inputs, cond_inputs=None, mode='direct'):
        if mode == 'direct':
            w = torch.tril(self.tri1).mm(torch.triu(self.tri2))
            u = (inputs - self.b).mm(w)
            return u
        else:
            w = torch.inverse(self.tri1.mm(self.tri2))
            x = inputs.mm(w) + self.b
            return x


class Sequential(nn.Sequential):
    """ A sequential container for net layers.
    In addition to a forward pass it implements a backward pass.
    """

    def forward(self, inputs, cond_inputs=None, mode='direct'):
        """ Performs a forward or backward pass for net modules.
        Args:
            inputs: inputs vectors
            mode: to run direct computation or inverse
        """
        self.num_inputs = inputs.size(-1)

        assert mode in ['direct', 'inverse']
        if mode == 'direct':
            for module in self._modules.values():
                inputs  = module(inputs, cond_inputs, mode)
        else:
            for module in reversed(self._modules.values()):
                inputs = module(inputs, cond_inputs, mode)

        return inputs


    def MLE(self, inputs, mean, var):
        device = inputs.device
        # forward pass
        u  = self(inputs)

        # Compute MLE loss
        log_det_sigma = torch.log(
            var + 1e-15).sum(-1, keepdim=True).to(device)

        log_probs = -0.5 * ((torch.pow((u - mean), 2) / (var + 1e-15) + torch.log(
            2 * pi )).sum(-1, keepdim=True) + log_det_sigma).to(device)

        loss = -(log_probs).mean()

        return u, loss

