import torch
import torch.utils.data as data
import os
import numpy as np
import copy

class data_loader(data.Dataset):
    def __init__(self, dataset_name, data_npz_path, raw_npz_path='raw_vector.npz', mode='train'):
        assert os.path.exists(data_npz_path) == True

        self.mode = mode
        self.dataset_name = dataset_name

        vector_data = np.load(data_npz_path)['vectors']
        spker_label = np.load(data_npz_path)['spker_label']
        utt_label = np.load(data_npz_path)['utt_label']

        self.vector_data = vector_data.astype(np.float32) # vectors
        self.spker_label = spker_label  # spker label
        self.utt_label = utt_label      # utt label

        if self.mode == 'train':
            spk2uk, spk2vk = compute_stats(raw_npz_path)
            self.spk2uk = spk2uk
            self.spk2vk = spk2vk

        print("dataset: {}".format(dataset_name))
        print("vecs shape: ", np.shape(vector_data))
        print("spker label shape: ", np.shape(spker_label))
        print("num of spker: ", np.shape(np.unique(spker_label)))
        print("utt label shape: ", np.shape(utt_label))
        print("num of utt: ", np.shape(np.unique(utt_label)))


    def __len__(self):
        return len(self.vector_data)


    def __getitem__(self, index):
        if self.mode == 'train':
            return self.vector_data[index], self.spk2uk[self.spker_label[index]], self.spk2vk[self.spker_label[index]]
        elif self.mode == 'eval':
            return self.vector_data[index]
        else:
            raise Exception('Mode Error')
            return

    @property
    def data(self):
        return self.vector_data

    @property
    def label(self):
        return self.spker_label


def compute_stats(npz):
    '''
    compute speaker-dependent class mean(uk) and variance(vk)
    '''
    assert os.path.exists(npz)
    vectors = np.load(npz)['vectors']
    spkers = np.load(npz)['spker_label']
    utters = np.load(npz)['utt_label']

    # spk2utt_vecs
    spk2utt = {}
    for idx in range(len(spkers)):
        spk = spkers[idx]
        if spk not in spk2utt:
            spk2utt[spk] = []
        vec = vectors[idx]
        spk2utt[spk].append(vec)

    # Compute SW, SB, spk2mean
    SB = []
    SW = np.zeros(np.shape(vectors)[1], dtype=float)
    spk2mean = {}
    for key, val in spk2utt.items():
        vecs = np.array(val)
        mean = np.mean(vecs, axis=0)
        spk2mean[key] = mean
        SB.append(mean)
        SW += len(vecs) * np.var(vecs, axis=0)

    SB = np.var(np.array(SB), axis=0)
    SW = SW / len(vectors)

    # Compute uk and vk for each spk
    spk2uk = {}
    spk2vk = {}
    for key, val in spk2utt.items():
        vecs = np.array(val)
        mean = spk2mean[key]
        num_utt = len(vecs)

        uk = mean * (num_utt * SB / (num_utt * SB + SW))
        vk = SW + SB * SW / (num_utt * SB + SW)

        spk2uk[key] = uk
        spk2vk[key] = vk

    return spk2uk, spk2vk

