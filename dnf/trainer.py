import os
import re
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
import kaldi_io
from tqdm import tqdm
import mle as fnn
from data_loader import *
from monitor import *
from tensorboardX import SummaryWriter

pi = torch.from_numpy(np.array(np.pi))

class trainer(object):
    def __init__(self, args):
        self.args = args
        # init cuda
        args.cuda = not args.no_cuda and torch.cuda.is_available()
        if args.cuda:
            os.environ["CUDA_VISIBLE_DEVICES"] = args.device
            self.device = torch.device("cuda:" + args.device)
            torch.cuda.manual_seed(args.seed)
        else:
            self.device = torch.device("cpu")
            torch.manual_seed(args.seed)
        print("torch device: {}".format(self.device))

        # init model
        num_inputs = args.num_inputs
        num_cond_inputs = None

        modules = []

        for _ in range(args.num_blocks):
            modules += [
                fnn.LinearLayer(num_inputs)
            ]
        self.model = fnn.Sequential(*modules)

        for module in self.model.modules():
            if isinstance(module, nn.Linear):
                nn.init.orthogonal_(module.weight)
                if hasattr(module, 'bias') and module.bias is not None:
                    module.bias.data.fill_(0)

        # init optimizer
        self.optimizer = optim.Adam(self.model.parameters(), lr=args.lr)


    def train(self):
        '''training process'''
        args = self.args
        kwargs = {'num_workers': 8, 'pin_memory': True}

        # tensorboardX
        self.writer = SummaryWriter(comment=args.log_dir)
        self.global_step = 0

        # init dataloader
        self.dataset = data_loader(
            dataset_name=args.dataset_name, data_npz_path=args.dev_data_npz, raw_npz_path=args.raw_data_npz)
        self.train_loader = torch.utils.data.DataLoader(
            self.dataset, batch_size=args.batch_size, shuffle=True, **kwargs)

        # init model
        self.reload_checkpoint()
        self.model.to(self.device)
        self.model.train()

        print('Dim of input : ', args.num_inputs)
        print('Dim of vector : ', np.shape(self.dataset.data)[1])
        assert args.num_inputs == np.shape(self.dataset.data)[1]

        # main to train
        start_epoch = self.epoch_idx

        if start_epoch >= args.epochs:
            print('Training Done.')
            return

        # epoch
        for idx in range(start_epoch, args.epochs):
            self.epoch_idx = idx
            # loss manager
            train_loss = AverageMeter()

            f_log = open(args.log_dir + os.sep + 'train_loss.log', 'a')

            # tqdm
            pbar = tqdm(total=len(self.train_loader.dataset))
            for batch_idx, (data, uk, vk) in enumerate(self.train_loader):
                data = data.to(self.device)
                uk = uk.to(self.device)
                vk = vk.to(self.device)
                self.optimizer.zero_grad()

                # compute MLE loss
                _, loss = self.model.MLE(data, uk, vk)
                train_loss.update(loss.item(), data.size(0))

                # update params
                loss.backward()
                self.optimizer.step()

                pbar.update(data.size(0))
                pbar.set_description('Loss val = {:.6f}'.format(train_loss.val))

                self.writer.add_scalar('MLE Loss', loss.item(), self.global_step)
                self.global_step += 1

                # average loss of mini-batch training process.
                f_log.write('Batch_idx {} : Total Loss val = {:.6f}\n'.format(
                    batch_idx, train_loss.val))

            pbar.close()

            # average loss in this epoch
            print('Epoch {} : MLE Loss = {:.6f}'.format(idx, train_loss.avg))
            f_log.write('Epoch {} : MLE Loss = {:.6f}\n'.format(idx, train_loss.avg))

            f_log.close()
        
            if self.epoch_idx % args.ckpt_save_interval == 0:
                self.save_checkpoint()

        self.save_checkpoint()
        print("Training Done.")


    def generate_z_npz(self):
        '''generate z and save as npz format'''
        args = self.args
        kwargs = {'num_workers': 12, 'pin_memory': True}
        # init model
        if args.infer_epoch == -1:
            self.reload_checkpoint()
        else:
            ckpt_path = '{}/ckpt_epoch{}.pt'.format(args.ckpt_dir, args.infer_epoch)
            print(ckpt_path)
            assert os.path.exists(ckpt_path) == True
            checkpoint_dict = torch.load(ckpt_path, map_location=self.device)
            self.model.load_state_dict(checkpoint_dict['model'])
            print("successfully reload {} [model] to infer".format(ckpt_path))

        self.model.to(self.device)
        self.model.eval()

        # init dataset
        dataset = data_loader(
            dataset_name=args.dataset_name, data_npz_path=args.test_data_npz, mode='eval')
        test_loader = torch.utils.data.DataLoader(
            dataset, batch_size=args.batch_size, shuffle=False, **kwargs)

        with torch.no_grad():
            pbar = tqdm(total=len(dataset.label))
            for batch_idx, data in enumerate(test_loader):
                data = data.to(self.device)
                z = self.model(data)
                z = z.cpu().detach().numpy()
                if batch_idx == 0:
                    vectors = z
                else:
                    vectors = np.vstack((vectors, z))
                pbar.update(data.size(0))
                pbar.set_description('vectors.npz generating {} / {}'.format(np.shape(vectors)[0], len(dataset.label)))
            pbar.close()

        if not os.path.exists(os.path.dirname(args.npz_dir)):
            os.makedirs(os.path.dirname(args.npz_dir))
        npz_path = args.npz_dir
        np.savez(npz_path, vectors=vectors, spker_label=dataset.spker_label, utt_label=dataset.utt_label)
        print("successfully save npz in {}".format(npz_path))


    def reload_checkpoint(self):
        '''check if checkpoint exists and reload the lastest checkpoint'''
        args = self.args
        self.epoch_idx = 0
        if not os.path.exists(args.ckpt_dir):
            os.makedirs(args.ckpt_dir)
            print("can not find ckpt dir, and create {} dir".format(args.ckpt_dir))
            print("start to train from epoch 0...")
        else:
            files = os.listdir(args.ckpt_dir)
            ckpts = []
            for f in files:
                if (f.endswith(".pt")):
                    ckpts.append(f)
            # load the lastest ckpt
            if (len(ckpts)):
                for ckpt in ckpts:
                    ckpt_epoch = int(re.findall(r"\d+", ckpt)[0])
                    if ckpt_epoch > self.epoch_idx:
                        self.epoch_idx = ckpt_epoch

                checkpoint_dict = torch.load(
                    '{}/ckpt_epoch{}.pt'.format(args.ckpt_dir, self.epoch_idx), map_location=self.device)

                self.model.load_state_dict(checkpoint_dict['model'])
                print("sucessfully reload ckpt_epoch{}.pt [model]".format(self.epoch_idx))

                self.optimizer.load_state_dict(checkpoint_dict['optimizer'])
                print("sucessfully reload ckpt_epoch{}.pt [optimizer]".format(self.epoch_idx))
                # load optimizer from no-cuda to cuda
                for state in self.optimizer.state.values():
                    for k, v in state.items():
                        if torch.is_tensor(v):
                            state[k] = v.cuda()

                self.epoch_idx += 1
            else:
                print("start to train from epoch 0...")


    def save_checkpoint(self):
        '''save checkpoints, including model, optimizer and class means'''
        args = self.args
        if not os.path.exists(args.ckpt_dir):
            os.makedirs(args.ckpt_dir)
            print("create ckpt dir {}".format(args.ckpt_dir))

        print("Saving model to {}/ckpt_epoch{}.pt".format(args.ckpt_dir, self.epoch_idx))
        torch.save({
            'model': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict()
        }, '{}/ckpt_epoch{}.pt'.format(args.ckpt_dir, self.epoch_idx))


if __name__ == "__main__":
    pass
