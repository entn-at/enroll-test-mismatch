import argparse

def get_args():
    parser = argparse.ArgumentParser(description='PyTorch Enroll-Test Mismatch')

    # controller
    parser.add_argument(
        '--dataset-name',
        default='dataX',
        help='dataset name')

    parser.add_argument(
        '--eval',
        action='store_true',
        default=False,
        help='process on eval or train')

    parser.add_argument(
        '--ckpt-dir',
        default='ckpt',
        help='dir to save check points')

    parser.add_argument(
        '--num-blocks',
        type=int,
        default=1,
        help='number of invertible blocks (default: 10)')

    parser.add_argument(
        '--num-inputs',
        type=int,
        default=-1,
        help='number of inputs')


    # training options
    parser.add_argument(
        '--raw-data-npz',
        default='raw/xvector.npz',
        help='path of raw (enroll condition) data npz')

    parser.add_argument(
        '--dev-data-npz',
        default='dev/xvector.npz',
        help='path of dev (test condition) data npz')

    parser.add_argument(
        '--batch-size',
        type=int,
        default=10000,
        help='input batch size for training (default: 10000)')

    parser.add_argument(
        '--epochs',
        type=int,
        default=100,
        help='number of epochs to train (default: 100)')

    parser.add_argument(
        '--lr',
        type=float,
        default=0.001,
        help='learning rate (default: 0.001)')

    parser.add_argument(
        '--no-cuda',
        action='store_true',
        default=False,
        help='disables CUDA training')

    parser.add_argument(
        '--seed',
        type=int,
        default=1,
        help='random seed (default: 1)')

    parser.add_argument(
        '--device',
        default='0',
        help='cuda visible devices (default: 0)')

    parser.add_argument(
        '--ckpt-save-interval',
        type=int,
        default=10,
        help='how many epochs to wait before saving models')

    parser.add_argument(
        '--log-dir',
        default='log',
        help='log-dir to save training status')


    # inference options
    parser.add_argument(
        '--test-data-npz',
        default='data/xvector.npz',
        help='path of infer data npz')

    parser.add_argument(
        '--infer-epoch',
        type=int,
        default=-1,
        help='index of ckpt epoch to infer (default: 100)')

    parser.add_argument(
        '--npz-dir',
        default='npz_data',
        help='infer npz dir')

    args = parser.parse_args()

    return args

